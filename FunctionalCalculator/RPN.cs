﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public static class RPN
    {
        public static string GetRPN(string expression)
        {
            if (string.IsNullOrEmpty(expression))
                throw new Exception("Expression isn't specified.");

            while (expression.Contains(' '))
            {
                expression = expression.Replace(" ", string.Empty);
            }

            const string leftBracket = "(";
            const string rightBracket = ")";

            // Выходная строка, разбитая на операции и операнды..
            var resultList = new Stack<string>();

            // Стек операций.
            var stack = new Stack<string>();

            var operationSymbols = Constants.MathOperations.Select(o => o.Key).ToList();

            operationSymbols.Add(leftBracket);
            operationSymbols.Add(rightBracket);
            
             // Индекс, на котором закончился разбор строки на прошлой итерации.
             var index = 0;

            // Признак необходимости поиска следующего элемента.
            var findNext = true;

            while (findNext)
            {
                var nextOperationIndex = expression.Length;
                var nextOperation = string.Empty;

                // Поиск следующего оператора или скобки.
                foreach (var operation in operationSymbols.ToArray())
                {
                    var i = expression.IndexOf(operation, index, StringComparison.Ordinal);

                    if (i < 0 || i >= nextOperationIndex) continue;

                    nextOperation = operation;
                    nextOperationIndex = i;
                }

                // Оператор не найден.
                if (nextOperationIndex == expression.Length)
                {
                    findNext = false;
                }
                else
                {
                    // Если оператору или скобке предшествует операнд, добавляем его в выходную строку.
                    if (index != nextOperationIndex)
                    {
                        resultList.Push(expression.Substring(index, nextOperationIndex - index));
                    }

                    // Обработка операторов и скобок.

                    if (nextOperation == leftBracket)
                    {
                        stack.Push(nextOperation);
                    }

                    else if (nextOperation == rightBracket)
                    {
                        while (stack.Peek() != leftBracket)
                        {
                            resultList.Push(stack.Pop());

                            if (!stack.Any())
                            {
                                throw new Exception("Unmatched brackets");
                            }
                        }

                        stack.Pop();
                    }
                    // Операция.
                    else
                    {
                        while (stack.Any() && stack.Peek() != leftBracket &&
                               Constants.MathOperations[nextOperation] >= Constants.MathOperations[stack.Peek()])
                        {
                            resultList.Push(stack.Pop());
                        }

                        stack.Push(nextOperation);
                    }

                    index = nextOperationIndex + nextOperation.Length;
                }
            }

            // Добавление в выходную строку операндов после последнего операнда.
            if (index != expression.Length)
            {
                resultList.Push(expression.Substring(index));
            }

            // Пробразование выходного списка к выходной строке.
            while (stack.Any())
            {
                resultList.Push(stack.Pop());
            }

            return string.Join(" ", resultList.Reverse().ToArray());
        }
    }
}
