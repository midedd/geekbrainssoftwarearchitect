using System;
using Calculator;
using NUnit.Framework;
using Void = Calculator.Void;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase("5+(6+2)*2", "5 6 2 + 2 * +")]
        [TestCase("5+(6/2)-2", "5 6 2 / + 2 -")]
        [TestCase("5*2 + 6/3", "5 2 * 6 3 / +")]
        public void BuildRpn(string expression, string result)
        {
            Assert.That(RPN.GetRPN(expression), Is.EqualTo(result));
        }

        [Test]
        [TestCase("5 6 2 + 2 * +", 21)]
        [TestCase("5 6 2 / + 2 -", 6)]
        [TestCase("5 2 * 6 3 / +", 12)]
        public void CalcRpn(string rpn, decimal result)
        {
            Assert.That(RpnCalculator.Calculate(rpn), Is.EqualTo(result));
        }

        [Test]
        [TestCase("5+(6+2)*2", 21)]
        [TestCase("5+(6/2)-2", 6)]
        [TestCase("5*2 + 6/3", 12)]
        public void TestChain(string expression, decimal result)
        {
            Assert.That(

                Chain<string>
                    .Init(expression)
                    .Map(RPN.GetRPN)
                    .Map(RpnCalculator.Calculate)
                    .Run(),
                
                Is.EqualTo(result));
        }
    }
}
