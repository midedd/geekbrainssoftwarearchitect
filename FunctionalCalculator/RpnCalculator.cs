﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public static class RpnCalculator
    {
        public static decimal Calculate(string rpnExpression)
        {
            var tokens = rpnExpression.Split(' ');
            var stack = new Stack<decimal>();

            foreach (var token in tokens)
            {
                if (!Constants.MathOperations.ContainsKey(token))
                {
                    stack.Push(decimal.Parse(token));
                }
                else
                {
                    var operand2 = stack.Pop();
                    var operand1 = stack.Any() ? stack.Pop() : 0;

                    switch (token)
                    {
                        case "*":
                            stack.Push(operand1 * operand2);
                            break;
                        case "/":
                            stack.Push(operand1 / operand2);
                            break;
                        case "+":
                            stack.Push(operand1 + operand2);
                            break;
                        case "-":
                            stack.Push(operand1 - operand2);
                            break;
                        default:
                            throw new Exception("Unknown operator");
                    }
                }
            }

            if (stack.Count != 1)
                throw new Exception("Expression syntax error.");

            return stack.Pop();
        }
    }
}
