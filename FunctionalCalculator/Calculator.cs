﻿using System;

namespace Calculator
{
    class Calculator
    {
        static void Main()
        {
            Chain<Void>
                
                .Init()

                .MapToVoid(i =>
                {
                    Console.Write("Enter expression: ");
                })

                .Map(i => Console.ReadLine())

                .Map(RPN.GetRPN)

                .Map(rpn =>
                {
                    Console.WriteLine($"RPN: {rpn}");
                    return rpn;
                })

                .Map(RpnCalculator.Calculate)

                .MapToVoid(result =>
                {
                    Console.WriteLine($"Result {result}");
                    Console.WriteLine("----------------------------");
                    Console.WriteLine("Press any key to exit");
                    Console.ReadKey();
                })

                .Run();
        }
    }
}
