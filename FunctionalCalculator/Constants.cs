﻿using System.Collections.Generic;

namespace Calculator
{
    public static class Constants
    {
        public static Dictionary<string, int> MathOperations = new Dictionary<string, int>
        {
            {"*", 1},
            {"/", 1},
            {"+", 2},
            {"-", 2}
        };
    }
}
