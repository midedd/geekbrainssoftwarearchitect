﻿using System;

namespace Calculator
{
    public abstract class Void { }

    public class Chain<T>
    {
        private readonly Action<Chain<T>> _action;

        private T _data;

        public static Chain<T> Init(T data = default)
        {
            return new Chain<T> {_data = data};
        }

        public Chain(Action<Chain<T>> action)
        {
            _action = action;
        }

        private Chain() { }

        public Chain<T2> Map<T2>(Func<T, T2> action)
        {
            return new Chain<T2>(bar =>
            {
                _action?.Invoke(this);
                bar._data = action.Invoke(_data);
            });
        }

        public Chain<T> MapToVoid(Action<T> action)
        {
            return new Chain<T>(bar =>
            {
                _action?.Invoke(this);
                action.Invoke(_data);
            });
        }

        public T Run()
        {
            _action.Invoke(this);
            return _data;
        }
    }
}
