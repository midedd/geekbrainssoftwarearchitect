using Calculator;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase("5+(6+2)*2", "5 6 2 + 2 * +")]
        [TestCase("5+(6/2)-2", "5 6 2 / + 2 -")]
        [TestCase("5*2 + 6/3", "5 2 * 6 3 / +")]
        public void BuildRpn(string expression, string result)
        {
            var rpn = new ReversePolishNotation(expression).ResultBySortingStation;
            
            Assert.That(rpn, Is.EqualTo(result));
        }

        [Test]
        [TestCase("5 6 2 + 2 * +", 21)]
        [TestCase("5 6 2 / + 2 -", 6)]
        [TestCase("5 2 * 6 3 / +", 12)]
        public void CalcRpn(string rpn, decimal result)
        {
            var calculation = new RpnCalculator(rpn).Result;

            Assert.That(result, Is.EqualTo(calculation));
        }
    }
}
