﻿using Calculator.Collections;

namespace Calculator
{
    public static class Constants
    {
        public const int DictionaryInitialSize = 10;

        public static MyDictionary<string, int> MathOperations = new MyDictionary<string, int>(4);

        static Constants()
        {
            MathOperations.Add("*", 1);
            MathOperations.Add("/", 1);
            MathOperations.Add("+", 2);
            MathOperations.Add("-", 2);
        }
    }
}
