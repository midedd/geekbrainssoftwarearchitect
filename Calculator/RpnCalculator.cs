﻿using System;
using Calculator.Collections;

namespace Calculator
{
    public class RpnCalculator
    {
        public decimal Result => _result ??= CalculateExpression();

        private readonly string _rpnExpression;

        private decimal? _result;

        public RpnCalculator(string rpnExpression)
        {
            _rpnExpression = rpnExpression;
        }

        public decimal CalculateExpression()
        {
            var tokens = _rpnExpression.Split(' ');
            var stack = new MyStack<decimal>();

            foreach (var token in tokens)
            {
                if (!Constants.MathOperations.ContainsKey(token))
                {
                    stack.Push(decimal.Parse(token));
                }
                else
                {
                    var operand2 = stack.Pop();
                    var operand1 = stack.Any() ? stack.Pop() : 0;

                    switch (token)
                    {
                        case "*":
                            stack.Push(operand1 * operand2);
                            break;
                        case "/":
                            stack.Push(operand1 / operand2);
                            break;
                        case "+":
                            stack.Push(operand1 + operand2);
                            break;
                        case "-":
                            stack.Push(operand1 - operand2);
                            break;
                        default:
                            throw new Exception("Unknown operator");
                    }
                }
            }

            if (stack.Count != 1)
                throw new Exception("Expression syntax error.");

            return stack.Pop();
        }
    }
}
