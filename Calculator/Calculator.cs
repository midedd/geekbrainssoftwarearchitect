﻿using System;

namespace Calculator
{
    class Calculator
    {
        static void Main(string[] args)
        {
            Console.WriteLine("----------------------------");
            Console.Write("Enter expression: ");

            var expression = Console.ReadLine();

            var rpn = new ReversePolishNotation(expression).ResultBySortingStation;

            Console.WriteLine($"RPN: {rpn}");

            var result = new RpnCalculator(rpn).Result;

            Console.WriteLine($"Result {result}");
            Console.WriteLine("----------------------------");
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

    }
}
