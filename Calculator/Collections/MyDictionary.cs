﻿using System;

namespace Calculator.Collections
{
    public class MyDictionary<TKey, TValue>
    {
        public TKey[] Keys;

        public TValue[] Values;

        public int Count;

        public MyDictionary(int initialSize = Constants.DictionaryInitialSize)
        {
            Keys = new TKey[initialSize];
            Values = new TValue[initialSize];
        }

        public void Add(TKey key, TValue value)
        {
            if (ContainsKey(key))
            {
                throw new Exception("Key already exists");
            }

            CheckAndResize();

            Keys[++Count - 1] = key;
            Values[Count - 1] = value;
        }

        private void CheckAndResize()
        {
            var length = Keys.GetLength(0);

            if (Keys.GetLength(0) == Count)
            {
                var newLength = (int) Math.Truncate(length * 1.5);
                Array.Resize(ref Keys, newLength);
                Array.Resize(ref Values, newLength);
            }
        }

        public bool ContainsKey(TKey key)
        {
            for (var i = 0; i < Keys.GetLength(0); i++)
            {
                if (Equals(Keys[i], key))
                    return true;
            }

            return false;
        }

        private int GetIndexByKey(TKey key)
        {
            for (var i = 0; i < Keys.GetLength(0); i++)
            {
                if (Equals(Keys[i], key))
                    return i;
            }

            throw new Exception("Key not found");
        }

        public TValue this[TKey key]
        {
            get
            {
                var index = GetIndexByKey(key);
                return Values[index];
            }
            set
            {
                var index = GetIndexByKey(key);
                Values[index] = value;
            }
        }

    }
}
