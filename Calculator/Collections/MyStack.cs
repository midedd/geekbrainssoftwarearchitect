﻿using System;

namespace Calculator.Collections
{
    public class MyStack<T>
    {
        private class Node<TNode>
        {
            public Node<TNode> Next;
            public TNode Data;
            public int Count;
        }

        public int Count => _top.Count;

        private Node<T> _top;

        public T Pop()
        {
            if (_top == null)
            {
                throw new InvalidOperationException("The stack is empty");
            }

            var data = _top.Data;
            _top = _top.Next;
            return data;
        }

        public void Push(T i)
        {
            var n = new Node<T>
            {
                Data = i, 
                Next = _top, 
                Count = (_top?.Count ?? 0) + 1, 
            };

            _top = n;
        }

        public bool Any()
        {
            return _top != null;
        }

        public T Peek()
        {
            if (_top == null)
            {
                throw new InvalidOperationException("The stack is empty");
            }

            return _top.Data;
        }

        public T[] ToArray()
        {
            if (_top == null)
            {
                return new T[0];
            }

            var arr = new T[_top.Count];

            var current = _top;
            var i = _top.Count - 1;

            while (current != null)
            {
                arr[i--] = current.Data;

                current = current.Next;
            }

            return arr;
        }

        public void AddRange(T[] range)
        {
            foreach (var value in range)
            {
                Push(value);
            }
        }
    }
}
