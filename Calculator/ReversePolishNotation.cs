﻿using System;
using Calculator.Collections;

namespace Calculator
{
    public class ReversePolishNotation
    {
        public string ResultBySortingStation => _resultBySortingStation ??= GetBySortingStation();

        private string _expression;

        private string _resultBySortingStation;

        public ReversePolishNotation(string expression)
        {
            _expression = expression;

            if (string.IsNullOrEmpty(_expression))
                throw new Exception("Expression isn't specified.");

            while (_expression.Contains(' '))
            {
                _expression = _expression.Replace(" ", string.Empty);
            }
        }

        private string GetBySortingStation()
        {
            const string leftBracket = "(";
            const string rightBracket = ")";

            // Выходная строка, разбитая на операции и операнды..
            var resultList = new MyStack<string>();

            // Стек операций.
            var stack = new MyStack<string>();

            _expression = _expression.Replace(" ", "");

            var operationSymbols = new MyStack<string>();

            operationSymbols.Push(leftBracket);
            operationSymbols.Push(rightBracket);
            
            operationSymbols.AddRange(Constants.MathOperations.Keys);

             // Индекс, на котором закончился разбор строки на прошлой итерации.
             var index = 0;

            // Признак необходимости поиска следующего элемента.
            var findNext = true;

            while (findNext)
            {
                var nextOperationIndex = _expression.Length;
                var nextOperation = "";

                // Поиск следующего оператора или скобки.
                foreach (var operation in operationSymbols.ToArray())
                {
                    var i = _expression.IndexOf(operation, index, StringComparison.Ordinal);

                    if (i < 0 || i >= nextOperationIndex) continue;

                    nextOperation = operation;
                    nextOperationIndex = i;
                }

                // Оператор не найден.
                if (nextOperationIndex == _expression.Length)
                {
                    findNext = false;
                }
                else
                {
                    // Если оператору или скобке предшествует операнд, добавляем его в выходную строку.
                    if (index != nextOperationIndex)
                    {
                        resultList.Push(_expression.Substring(index, nextOperationIndex - index));
                    }

                    // Обработка операторов и скобок.

                    if (nextOperation == leftBracket)
                    {
                        stack.Push(nextOperation);
                    }

                    else if (nextOperation == rightBracket)
                    {
                        while (stack.Peek() != leftBracket)
                        {
                            resultList.Push(stack.Pop());

                            if (!stack.Any())
                            {
                                throw new Exception("Unmatched brackets");
                            }
                        }

                        stack.Pop();
                    }
                    // Операция.
                    else
                    {
                        while (stack.Any() && stack.Peek() != leftBracket &&
                               Constants.MathOperations[nextOperation] >= Constants.MathOperations[stack.Peek()])
                        {
                            resultList.Push(stack.Pop());
                        }

                        stack.Push(nextOperation);
                    }

                    index = nextOperationIndex + nextOperation.Length;
                }
            }

            // Добавление в выходную строку операндов после последнего операнда.
            if (index != _expression.Length)
            {
                resultList.Push(_expression.Substring(index));
            }

            // Пробразование выходного списка к выходной строке.
            while (stack.Any())
            {
                resultList.Push(stack.Pop());
            }

            return string.Join(" ", resultList.ToArray());
        }
    }
}
