/* Т.к. есть однофамильцы, то нельзя сопоставить банковский счет или 
   транспортное средство только с фамилией.
   Поэтому введено имя как часть составного ключа (имя+фамилия).
   Использование имеющихся полей для уникальности, например, 
   добавление города в автомобиль, не сочетается с предметной областью.
   
  
  ЗАДАНИЕ A:
  ?- car_by_phone('+3413431431', FirstName, SecondName, Brand, Model, Price).
  
  Результат:
    	
		FirstName = 'Norah',
    	SecondName = 'Johnes'
    	Brand = 'Lada',
		Model = '4x4',
		Price = price('1000', 'USD'),
		
		FirstName = 'Norah',
	    SecondName = 'Johnes'
	    Brand = 'Plimouth',
		Model = 'Fury',
		Price = price('150000', 'USD'),	
  
  ЗАДАНИЕ B:
  ?- car_by_phone('+3413431431', _, _, Brand, Model, _).
  
  Результат:
  
		Brand = 'Lada',
		Model = '4x4'

		Brand = 'Plimouth',
		Model = 'Fury'
  
  ЗАДАНИЕ C:
  
  Тест 1:
  ?- person(FirstName, 'Johnes', Phone, address('NY', Street, _, _)), account(Bank, FirstName, SecondName, _).
  
  Должна найтись Norah Jones.
  
  Результат:
  
		Bank = 'Barclays',
		FirstName = 'Norah',
		SecondName = 'Johnes',
        Phone = '+3413431431',
		Street = 'Broadway'
        
		Bank = 'City',
		FirstName = 'Norah',
		SecondName = 'Johnes',
        Phone = '+3413431431',
		Street = 'Broadway'
        
  Тест 2:
  ?- person(FirstName, 'Johnes', Phone, address('Washington DC', Street, _, _)), account(Bank, FirstName, SecondName, _).
         
  Долежен найтись Michael Jones.
  
  Результат:
		Bank = 'JP Morgan',
		FirstName = 'Michael',
		SecondName = 'Johnes',
        Phone = '+24342432',
		Street = 'Broadway'
 */

person('Norah', 'Johnes', '+3413431431', address('NY', 'Broadway', 4, 65)).
person('Mick', 'Jagger', '+12332432', address('Huston', '54st', 45, none)).
person('Bojack', 'Horsemen', '+199999999', address('LA', 'Hollywood Hills', 522, none)).
person('Michael', 'Johnes', '+24342432', address('Washington DC', 'Broadway', 4, 65)).

vehicle('Lada', 'Norah', 'Johnes', '4x4', black, price('1000', 'USD')).
vehicle('Plimouth', 'Norah', 'Johnes', 'Fury', brown, price('150000', 'USD')).
vehicle('Harley Davidson', 'Mick', 'Jagger', 'Hardtail', white, price('10000', 'USD')).
vehicle('Cadillac', 'Bojack', 'Horsemen', 'Escalade', white, price('40000', 'USD')).

account('Barclays', 'Norah', 'Johnes', balance('20000', 'USD')).
account('Bank of NY', 'Mick', 'Jagger', balance('1000000', 'USD')).
account('City', 'Mick', 'Jagger', balance('2000000', 'USD')).
account('City', 'Norah', 'Johnes', balance('30000', 'USD')).
account('Barclays', 'Bojack', 'Horseman', balance('100', 'USD')).
account('JP Morgan', 'Michael', 'Johnes', balance('10000', 'EUR')).

car_by_phone(Phone, FirstName, SecondName, Brand, Model, Price) :-
	person(FirstName, SecondName, Phone, _), 
    vehicle(Brand, FirstName, SecondName, Model, _, Price).